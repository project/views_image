<?php

class views_handler_field_image extends views_handler_field {

  function query() {
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['label']['default'] = '';

    $options['views_image_path'] = array('default' => '');
    $options['views_image_style'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['views_image'] = array();

    $form['views_image_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('You may use Drupal streams, for example <em>public://example.jpg</em>. External URLs are also allowed.'),
      '#default_value' => $this->options['views_image_path'],
      '#required' => TRUE,
    );

    $form['views_image_style'] = array(
      '#type' => 'select',
      '#title' => t('Style'),
      '#options' => image_style_options(),
      '#description' => t('Ignored if image path contains an external URL.'),
      '#default_value' => $this->options['views_image_style'],
    );

    unset($form['element_type']);
    unset($form['alter']['trim']);
    unset($form['alter']['strip_tags']);
    unset($form['alter']['strip_whitespace']);
    unset($form['alter']['nl2br']);
  }

  function render($values) {
    $variables = array(
      'path' => $this->options['views_image_path'],
    );

    if (empty($this->options['views_image_style'])
      || url_is_external($this->options['views_image_path'])) {
      $theme = 'image';
    }
    else {
      $theme = 'image_style';
      $variables['style_name'] = $this->options['views_image_style'];
    }

    return theme($theme, $variables);
  }
}

function views_image_views_data() {

  $data = array();

  $data['table'] = array(
    'group' => t('Global'),
    'join' => array('#global' => array()),
  );

  $data['image'] = array(
    'title' => t('Image'),
    'help' => t('Display an arbitrary image with optional style'),
    'field' => array(
      'handler' => 'views_handler_field_image',
    ),
  );

  return array('views_image' => $data);
}

